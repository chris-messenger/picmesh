picmesh.py
-----
This script is designed to take an input image or video and convert into a wire-frame or mesh-like version. The effect is analogous to representations of distorted (non-flat) space-times within Einstein's theory of General Relativity and is an Education and Public Outreach tool for the gravity and gravitational wave scientific community. 

Method
-----
The code first converts an input image into greyscale representing a 2D psuedo-potential. We then make 2 images that represent the x and y gradients of this potential and these define a vector field of forces.

A uniform spaced grid of points is then placed on this potential each connected to it's 4 square lattice neighbours. We model these connections as spring like with a particular spring constant. The points on the boundary are considered fixed.

The force on a given point in the lattice is then the sum of the underlying vector field defined by the image and the sum of the forces from each point's neighbours. The equations of motion for the points are evolved simultaneously through a relaxation method until an equalibrium is reached.

The resultant distribution of lattice points become denser in regions of greyscale image intensity and less dense in darker regions. The visual effect close to the image is that of a relatively formless mesh-like projection of a 3D structure. When viewed from further away the original image becomes apparent and appears to have a 3D quality.

Usage
-----
The user simply needs to supply an image or video as input. It is possible to choose the number of mesh points, the effective tension in the lattice connections, the output image resolution, the background and foreground colours, the mesh line widths, and to invert the image potential. 

Please run python picmesh.py -h for more help and options.



