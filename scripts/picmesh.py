##############################################################################
# Copyright (c) 2018 Chris Messenger
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#############################################################################

import numpy as np
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
from scipy.ndimage import imread
from scipy.misc import imresize
from scipy.interpolate import interp2d, RectBivariateSpline, bisplrep, bisplev
from skimage import exposure
from skimage.morphology import disk
from skimage.filters import rank
from matplotlib.collections import PatchCollection
from matplotlib.patches import Polygon
import os
import argparse
import time
import sys
import pylab
import imageio

def parser():
    """Parses command line arguments"""
    parser = argparse.ArgumentParser(prog='picmesh.py',description='converts an image/video into a 3D-like mesh image/video')

    # arguments for reading in a data file
    parser.add_argument('-i', '--infile', type=str, default=None, help='the input image/video')
    parser.add_argument('-N', '--nmesh', type=int, default=48, help='the number of mesh lines on the longest image axis')
    parser.add_argument('-t', '--tension', type=float, default=0.7, help='the mesh tension')
    parser.add_argument('-z', '--tol', type=float, default=1e-3, help='the RMS tolerance')
    parser.add_argument('-M', '--maxiter', type=int, default=3000, help='the maximum number of iterations')
    parser.add_argument('-d', '--dpi', type=int, default=300, help='the output image resolution')
    parser.add_argument('-l', '--lw', type=float, default=2.0, help='the mesh line thickness')
    parser.add_argument('-bc', '--bcolour', type=str, default='black', help='the background colour')
    parser.add_argument('-mc', '--mcolour', type=str, default='white', help='the mesh colour')
    parser.add_argument('-pc', '--pcolour', type=str, default=None, help='the patch colour palette')
    parser.add_argument('-p', '--patch', dest='patch', action='store_true', help='plot patches')
    parser.add_argument('-L', '--lens', dest='lens', action='store_true',help='apply gaussian lens')
    parser.add_argument('-s', '--lscale', type=float, default=0.5, help='the lens scale (fraction of nmesh)')
    parser.add_argument('-v', '--invert', dest='invert', action='store_true',help='invert the image effect')

    return parser.parse_args()

def plotmesh(x,Nx,Ny,filename,lw=1.0,invert=False,dpi=900,mcolour='black',bcolour='white',pcolour='blue'):
    """
    Plots the mesh - also colours in the patches if requested
    """
    scale = 10.0/Ny # scale the image so that the x-axis is 10 inches
    fig,ax = plt.subplots(1,figsize=(Nx*scale,10),facecolor=bcolour)    

    if pcolour is None and mcolour is None:
        print '{}" Error. Provide a colour for one or both of patch and/or mesh. Exiting'.format(time.asctime())
        exit(1)

    # draw all the patches 
    if pcolour is not None:
        nfloors = np.random.rand((Nx-2)*(Ny-2)) # some random data
        patches = []

        dx = 0.0
        dens = []
        for i in xrange(0,Nx-1):
            for j in xrange(0,Ny-1):
                if np.random.uniform(0,1,1)>0.0:
                    verts = np.array([x[i,j]+[dx,dx],x[i+1,j]+[-dx,dx],x[i+1,j+1]+[-dx,-dx],x[i,j+1]+[dx,-dx]])
                    L12 = np.sqrt((x[i,j,0]-x[i,j+1,0])**2 + (x[i,j,1]-x[i,j+1,1])**2)
                    L23 = np.sqrt((x[i,j+1,0]-x[i+1,j+1,0])**2 + (x[i,j+1,1]-x[i+1,j+1,1])**2)
                    L34 = np.sqrt((x[i+1,j+1,0]-x[i+1,j,0])**2 + (x[i+1,j+1,1]-x[i+1,j,1])**2)
                    L41 = np.sqrt((x[i+1,j,0]-x[i,j,0])**2 + (x[i+1,j,1]-x[i,j,1])**2)
                    L = np.sqrt((x[i,j,0]-x[i+1,j+1,0])**2 + (x[i,j,1]-x[i+1,j+1,1])**2)
                    sa = 0.5*(L12 + L23 + L)
                    sb = 0.5*(L41 + L34 + L)
                    temp = np.sqrt(sa*(sa-L12)*(sa-L23)*(sa-L)) + np.sqrt(sb*(sb-L41)*(sb-L34)*(sb-L))
                    dens.append(temp)
                    polygon = Polygon(verts,closed=True)
                    patches.append(polygon)
        
        dens = np.array(dens).astype('float')
        if np.all(dens==dens[0]):
            dens = 0.5
        else:
            dens -= np.min(dens)
            dens /= np.max(dens)
        if pcolour=='blue':
            cmap = plt.get_cmap('Blues')
        elif pcolour=='red':
            cmap = plt.get_cmap('Reds')
        elif pcolour=='grey':
            cmap = plt.get_cmap('Greys')
        elif pcolour=='purple':
            cmap = plt.get_cmap('Purples')
        elif pcolour=='green':
            cmap = plt.get_cmap('Greens')
        elif pcolour=='brg':
            cmap = plt.get_cmap('brg')
        elif pcolour=='hsv':
            cmap = plt.get_cmap('hsv')
        elif pcolour=='jet':
            cmap = plt.get_cmap('jet')
        else:
            print '{}: unknown colour {}. Exiting.'.format(time.asctime(),colour)
        colors = cmap(1.0-dens) # convert nfloors to colors that we can use later

        collection = PatchCollection(patches)
        ax.add_collection(collection)
        collection.set_color(colors)

    # plot mesh lines
    if mcolour is not None:
        for i in xrange(1,Nx-1):
            plt.plot(x[i,:,0],x[i,:,1],'-',color=mcolour,lw=lw)
        for j in xrange(1,Ny-1):
            plt.plot(x[:,j,0],x[:,j,1],'-',color=mcolour,lw=lw)

    plt.gca().invert_yaxis()
    plt.axis('equal')
    plt.gca().axes.get_xaxis().set_visible(False)
    plt.gca().axes.get_yaxis().set_visible(False)
    plt.gca().axis('off')

    # save to file if a single image
    if filename is not None:
        plt.savefig(filename,bbox_inches='tight',pad_inches=0,dpi=dpi,facecolor=fig.get_facecolor())
        plt.close()
        return
    else:               # otherwise return image data
        fig.canvas.draw()

        # Now we can save it to a numpy array.
        data = np.fromstring(fig.canvas.tostring_rgb(), dtype=np.uint8, sep='')
        
        data = data.reshape(fig.canvas.get_width_height()[::-1] + (3,))
        plt.close()
        return data

def evolvemesh(origpic,Nx,Ny,args,xstart=None,tol=1e-3,verb=True):
    """
    main part of the code that evolves the mesh 
    """

    # make initial mesh locations if needed
    if xstart is None:
        x = np.zeros((Nx,Ny,2))
        for a in xrange(Nx):
            for b in xrange(Ny):
                x[a,b,0] = a
                x[a,b,1] = b
        if verb:
            print '{}: initialised mesh'.format(time.asctime())
    else:
        x = xstart
        if verb:
            print '{}: using pre-initialised mesh'.format(time.asctime())

    # resize image
    img = imresize(origpic, [Nx,Ny], interp='bicubic').astype('float')
    if verb:
        print '{}: resized the image'.format(time.asctime())

    # if image is completely uniform then return input
    if np.all(img==img[0,0]):
        return x
    
    # equalise the image
    img -= np.min(img)
    img /= np.max(img)
    img = -1.0 + 2.0*img
    pic = exposure.equalize_hist(img)
    if verb:
        print '{}: equalized the image'.format(time.asctime())

    # stretch the values to span 0 - 1
    pic -= float(np.min(pic))
    pic /= float(np.max(pic))
    if verb:
        print '{}: checked for completely uniform image'.format(time.asctime())

    # compute image gradients - x and y directions
    i = np.arange(Nx-1)
    j = np.arange(Ny-1)
    ii, jj = np.meshgrid(i,j)
    gradx = (pic[ii+1,jj]-pic[ii,jj])
    grady = (pic[ii,jj+1]-pic[ii,jj])
    if verb:
        print '{}: computed image gradients'.format(time.asctime())
    
    # make interpolation object of image on new mesh space
    fx = RectBivariateSpline(i, j, gradx.transpose())
    fy = RectBivariateSpline(i, j, grady.transpose())
    if verb:
        print '{}: initialised interpolation'.format(time.asctime())

    # define gradient wrt x
    def dphix(x):
        return np.array(fx(x[:,:,0].flatten(),x[:,:,1].flatten(),grid=False)).reshape(Nx-2,Ny-2)

    # define gradient wrt y
    def dphiy(x):
        return np.array(fy(x[:,:,0].flatten(),x[:,:,1].flatten(),grid=False)).reshape(Nx-2,Ny-2)

    # define second derivative wrt x
    def dphix2(x):
        dx = np.array([1e-6,0.0])
        return 0.5*(dphix(x+dx)-dphix(x-dx))/dx[0]

    # define second derivative wrt y
    def dphiy2(x):
        dx = np.array([0.0,1e-6])
        return 0.5*(dphiy(x+dx)-dphiy(x-dx))/dx[1]

    # relax mesh based on image
    prob = 0.5                                  # chance of moving a particular mesh point
    Fdir = -1.0 if args.invert else 1.0         # the force direction from the image
    cnt = 0
    rms = tol + 1.0
    while rms>tol:

        # make mask - so we're not changing all point simulateously
        mask = np.array(np.random.uniform(0,1,(Nx-2,Ny-2))<0.5).astype('float')

        dxL = x[1:-1,1:-1,0]-x[:-2,1:-1,0] # get x distance from left neighbour
        dxR = x[1:-1,1:-1,0]-x[2:,1:-1,0]  # get x distance from right neighbour
        dyL = x[1:-1,1:-1,1]-x[:-2,1:-1,1] # get y distance from left neighbour
        dyR = x[1:-1,1:-1,1]-x[2:,1:-1,1]  # get y distance from right neighbour
        dxD = x[1:-1,1:-1,0]-x[1:-1,:-2,0] # get x distance from lower neighbour
        dxU = x[1:-1,1:-1,0]-x[1:-1,2:,0]  # get x distance from upper neighbour
        dyD = x[1:-1,1:-1,1]-x[1:-1,:-2,1] # get y distance from lower neighbour
        dyU = x[1:-1,1:-1,1]-x[1:-1,2:,1]  # get y distance from upper neighbour

        # actual function
        Fx = -args.tension*(dxL + dxR + dxU + dxD) + Fdir*dphix(x[1:-1,1:-1,:])        
        Fy = -args.tension*(dyL + dyR + dyU + dyD) + Fdir*dphiy(x[1:-1,1:-1,:])

        # gradients of function
        dFx = -4.0*args.tension + Fdir*dphix2(x[1:-1,1:-1,:])
        dFy = -4.0*args.tension + Fdir*dphiy2(x[1:-1,1:-1,:])

        # update the random subset of points - for stability only do subset
        x[1:-1,1:-1,0] -= mask*0.25*Fx/dFx
        x[1:-1,1:-1,1] -= mask*0.25*Fy/dFy

        # compute the rms update size
        rms = np.sqrt(np.mean((mask*0.25*Fx/dFx)**2 + (mask*0.25*Fy/dFy)**2)/prob)

        # output status
        if verb:
            width = 30
            n = int((width+1) * float(tol) / rms)
            sys.stdout.write("\r{0}: generating mesh [{1}{2}]".format(time.asctime(),'#' * n, ' ' * (width - n)))
    if verb:
        sys.stdout.write("\n")
 
    return x

def bandw(img):
    return img[:,:,0] * 299/1000.0 + img[:,:,1] * 587/1000.0 + img[:,:,2] * 114/1000.0

# the main part of the code
def main():
    """
    The main code
    """
     
    # get the command line args
    args = parser()

    # read in the file and make it black and white
    # if a video only read the first image
    basename = os.path.splitext(args.infile)[0]
    fileformat = os.path.splitext(args.infile)[1]
    if fileformat=='.mp4':
        vid = imageio.get_reader(args.infile, 'ffmpeg')
        fps = vid.get_meta_data()['fps']
        origpic = vid.get_data(10)
        origpic = bandw(origpic) 
    elif fileformat=='.png':
        origpic = imread(args.infile, flatten=True, mode=None)
    else:
        print '{}: sorry, unknown format {}. Exiting'.format(time.asctime(),fileformat)    
        exit(1)

    origpic = np.transpose(origpic)
    print '{}: read in image {}'.format(time.asctime(),args.infile)

    # compute new grid shape
    origNx,origNy = origpic.shape
    print '{}: input image size {},{}'.format(time.asctime(),origNx,origNy)
    if origNx>origNy:
        Nx = args.nmesh
        xscale = Nx/float(origNx)
        Ny = int(origNy*xscale)
        yscale = Ny/float(origNy)
    else:
        Ny = args.nmesh
        yscale = Ny/float(origNy)
        Nx = int(origNx*yscale)    
        xscale = Nx/float(origNx)
    print '{}: output image mesh will be {},{}'.format(time.asctime(),Nx,Ny)
    
    # process the original image
    x = evolvemesh(origpic,Nx,Ny,args,xstart=None,tol=args.tol)

    # if doing only a single image then output the result
    if fileformat=='.png':

        # make the final plot
        outfile = '{}_mesh.png'.format(basename)
        plotmesh(x,Nx,Ny,outfile,lw=args.lw,invert=args.invert,dpi=args.dpi,mcolour=args.mcolour,bcolour=args.bcolour,pcolour=args.pcolour)
        print '{}: saved output to {}'.format(time.asctime(),outfile)

    elif fileformat=='.mp4':
    
        # loop over each video image
        data = []
        lastx = x
        cnt = 0
        vid = imageio.get_reader(args.infile, 'ffmpeg')
        nframes = vid.get_length()
        for im in vid:

            # process the next image
            im = bandw(im)    
            im = np.transpose(im)
            x = evolvemesh(im,Nx,Ny,args,xstart=lastx,tol=args.tol,verb=False)
            
            # store processed frame
            temp = plotmesh(x,Nx,Ny,None,lw=args.lw,invert=args.invert,dpi=args.dpi,mcolour=args.mcolour,bcolour=args.bcolour,pcolour=args.pcolour)
            lastx = x   # store last mesh as a start point for next frame
            data.append(temp) 

            # output status
            width = 30
            n = int((width+1) * float(cnt) / nframes)
            sys.stdout.write("\r{0}: generating video mesh [{1}{2}]".format(time.asctime(),'#' * n, ' ' * (width - n)))
            cnt += 1
        sys.stdout.write("\n")

        # output to video file
        outfile = '{}_mesh.mp4'.format(basename)
        writer = imageio.get_writer(outfile, fps=fps)
        for im in data:
            writer.append_data(im)
        writer.close()
        
if __name__ == "__main__":
    exit(main())

